const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const knex = require('knex')({ client: 'mysql' });

app.use(bodyParser.json({ type: 'application/json' }));

const table = 'session';

// ! Known bugs in comments below

const constraintLookup = {
  [`equals`]: '=',
  [`does not equal`]: '='
  // [`in list`]:
  // [`not in list`]:

  // TODO: breakout function
  // [`less than or equal to`]:
  // [`greater than or equal`]:

  // TODO: breakout function to append %
  // [`starts with`]:
  // [`does not start with`]:
  // [`contains`]:
  // [`does not contain`]:
  // [`contains all`]:

  // TODO:
  // [`range`]:
};

app.post('/buildString', (req, res) => {
  const { body } = req.body;

  let sqlQuery = knex(table);
  body.forEach(constraint => {
    if (constraint.constraint === 'does not equal') {
      sqlQuery = sqlQuery.andWhere(function() {
        this.andWhereNot(
          constraint.columnId,
          constraintLookup[constraint.constraint],
          constraint.constraintValue
        );
      });
    }
    if (constraint.constraint === 'equals') {
      sqlQuery = sqlQuery.andWhere(function() {
        this.where(
          constraint.columnId,
          constraintLookup[constraint.constraint],
          constraint.constraintValue
        );
      });
    }
  });
  res.send(JSON.stringify(sqlQuery.toString()));
});

const port = process.env.PORT || 5555;

app.listen(port, () => {
  console.log(`Listening on PORT: ${port}`);
});
