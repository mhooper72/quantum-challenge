import React, { Component } from 'react';
import axios from 'axios';
import { Button, Message } from 'semantic-ui-react';
import FilterField from './FilterField';
import {
  getConstraintsForColumnId,
  placeholderStringFields,
  numericConstraints,
  stringFields
} from '../filterData';

// Simple StyleyButton for shorter syntax
const StyledButton = props => <Button {...props} />;

class FilterFieldContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterSelections: [{ ...placeholderStringFields }],
      sqlString: ''
    };
  }

  // check for empty contraint value fields and set error
  checkForEmptyValueFields = () => {
    this.state.filterSelections.forEach((selection, index) => {
      if (selection.constraintValue === '') {
        this.setState(prevState => {
          const newFilterSelections = prevState.filterSelections.map((filters, i) => {
            if (i !== index) return filters;
            return { ...filters, error: true };
          });
          return {
            filterSelections: newFilterSelections,
            error: true,
            sqlString: ''
          };
        });
      }
    });
  };

  // adds new filterField
  handleAddFilterField = () => {
    this.setState(prevState => ({
      filterSelections: [...prevState.filterSelections, { ...placeholderStringFields }],
      sqlString: ''
    }));
  };

  // handles selection of filter or constraintValue and resets on filter change
  handlePropertySelection = (index, property, value, shouldReset) => {
    // TODO: Clean up and make more efficient
    let resetContstraintValue;
    if (stringFields.includes(value)) {
      resetContstraintValue = `starts with`;
    } else {
      resetContstraintValue = 'range';
    }
    // TODO: Clean up and make more efficient / DRY
    this.setState(prevState => {
      if (shouldReset) {
        const newFilterSelections = prevState.filterSelections.map((filters, i) => {
          if (i !== index) return filters;
          return { ...filters, [property]: value, constraint: resetContstraintValue };
        });
        return {
          filterSelections: newFilterSelections,
          sqlString: ''
        };
      }
      const newFilterSelections = prevState.filterSelections.map((filters, i) => {
        if (i !== index) return filters;
        return { ...filters, [property]: value, error: false };
      });
      return {
        filterSelections: newFilterSelections,
        sqlString: '',
        error: false
      };
    });
  };

  handleRemoveFieldAtIndex = index => {
    this.setState(prevState => {
      // slice out the removed index
      const newFilterSelections = [
        ...prevState.filterSelections.slice(0, index),
        ...prevState.filterSelections.slice(index + 1)
      ];
      // Prevents having zero filters displayed
      if (newFilterSelections.length === 0)
        return { filterSelections: [{ ...placeholderStringFields }], sqlString: '' };
      return { filterSelections: newFilterSelections, sqlString: '' };
    });
  };

  handleBuildString = async () => {
    await this.checkForEmptyValueFields();
    // TODO: API Error checking !== '200'
    if (!this.state.error) {
      const options = {
        method: 'POST',
        body: this.state.filterSelections,
        headers: { 'Content-Type': 'application/json' }
      };
      try {
        axios.post('/buildString', options).then(res =>
          this.setState(() => ({
            sqlString: res.data
          }))
        );
      } catch (error) {
        console.log('there was an error :( ', error);
      }
    }
  };

  render() {
    const { filterSelections } = this.state;
    return (
      <div>
        {filterSelections.map((filterSelection, index) => {
          return (
            <FilterField
              columnId={filterSelection.columnId}
              constraint={filterSelection.constraint}
              error={filterSelection.error}
              handleConstraintsSelection={constraint =>
                this.handlePropertySelection(index, 'constraint', constraint)
              }
              handleConstraintsValueSelection={constraintValue =>
                this.handlePropertySelection(index, 'constraintValue', constraintValue)
              }
              handleSecondConstraintsValueSelection={constraintValue =>
                this.handlePropertySelection(index, 'secondConstraintValue', constraintValue)
              }
              handleDeleteSearchField={this.handleDeleteSearchField}
              handleFilterSelection={columnId =>
                this.handlePropertySelection(index, 'columnId', columnId, true)
              }
              handleRemove={() => this.handleRemoveFieldAtIndex(index)}
              key={`${index} + ${filterSelection.columnId}`}
              // shows extra range fields if contraint === 'range'
              showRangeInputs={filterSelection.constraint === 'range'}
              // if is numericConstraints, show numeric identifiers
              showNumeric={
                getConstraintsForColumnId(filterSelection.columnId) === numericConstraints ? (
                  <Button
                    content="is"
                    disabled
                    style={{ marginLeft: '.25em', marginRight: '-.25em', padding: '10px' }}
                  />
                ) : null
              }
            />
          );
        })}
        <StyledButton
          color="facebook"
          content="ADD"
          onClick={this.handleAddFilterField}
          style={{ marginTop: '1em' }}
        />
        <div>
          <StyledButton
            color="facebook"
            content="Search"
            onClick={this.handleBuildString}
            floated="right"
            style={{ width: '250px' }}
          />
        </div>
        <Message style={{ marginTop: '3em' }} hidden={this.state.sqlString === ''} info>
          <p>{this.state.sqlString}</p>
        </Message>
      </div>
    );
  }
}

export default FilterFieldContainer;
