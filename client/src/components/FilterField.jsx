import React, { Fragment } from 'react';
import { fields, getConstraintsForColumnId } from '../filterData';
import { Button, Icon, Input, Select } from 'semantic-ui-react';

// Creates dropdown for query filters
const FilterDropDown = ({ columnId, handleFilterSelection }) => (
  <Select
    defaultValue={columnId}
    onChange={(e, data) => handleFilterSelection(data.value)}
    options={fields.map(field => {
      const displayField = {
        key: field.columnId,
        value: field.columnId,
        text: field.displayName,
        id: field.columnId
      };
      return displayField;
    })}
    style={{ marginLeft: '.5em' }}
  />
);

// Creates dropdown for query constraints
const ConstraintDropDown = ({ columnId, constraint, handleConstraintsSelection }) => (
  <Select
    defaultValue={constraint}
    onChange={(e, data) => handleConstraintsSelection(data.value)}
    options={getConstraintsForColumnId(columnId).map(option => {
      const displayOption = {
        key: option,
        text: option,
        value: option,
        id: option
      };
      return displayOption;
    })}
    style={{ marginLeft: '.5em' }}
  />
);

const FilterField = ({
  columnId = fields[0].columnId,
  constraint,
  error,
  handleConstraintsSelection,
  handleConstraintsValueSelection,
  handleSecondConstraintsValueSelection,
  handleFilterSelection,
  handleRemove,
  showNumeric,
  showRangeInputs
}) => {
  // if costraints not selected, defaults to first constraint field
  if (!constraint) {
    constraint = getConstraintsForColumnId(columnId)[0];
  }
  return (
    <div style={{ marginBottom: '.75em', marginLeft: '.5em' }}>
      <Button style={{ paddingRight: '9px' }} onClick={handleRemove}>
        <Icon name="minus" />
      </Button>
      <FilterDropDown
        columnId={columnId}
        fluid
        handleFilterSelection={handleFilterSelection}
        inline
      />
      {/* if is numericConstraints, show numeric identifiers */}
      {showNumeric}
      <ConstraintDropDown
        inline
        columnId={columnId}
        constraint={constraint}
        handleConstraintsSelection={handleConstraintsSelection}
      />
      <Input
        error={error}
        onChange={e => handleConstraintsValueSelection(e.target.value)}
        placeholder={error ? `required` : columnId === 'user_email' ? `Enter email...` : ''}
        style={{ marginLeft: '.5em' }}
        type="text"
      />
      {/* move logic to container */}
      {showRangeInputs && (
        <Fragment>
          <Button disabled style={{ marginLeft: '.25em', marginRight: '-.25em', padding: '10px' }}>
            and
          </Button>
          <Input
            error={error}
            onChange={e => handleSecondConstraintsValueSelection(e.target.value)}
            style={{ marginLeft: '.5em' }}
            type="text"
          />
        </Fragment>
      )}
    </div>
  );
};

export default FilterField;
