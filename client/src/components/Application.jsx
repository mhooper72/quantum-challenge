import React from 'react';
import { Container, Divider } from 'semantic-ui-react';
import FilterFieldContainer from './FilterFieldContainer';
import { Button, Label } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

const Application = ({ buildString, stringBuilder: { clicks, loading, timer } }) => (
  <Container>
    <div className="App">
      <h2 style={{ marginTop: '2em' }}>SEARCH FOR SESSIONS</h2>
      <Divider />
      <FilterFieldContainer />
      <div
        style={{
          position: 'fixed',
          bottom: '3em'
        }}
      >
        {/* Silly redux button b/c this app really didn't need redux  */}
        <Button size="big" color="pink" loading={loading} onClick={buildString}>
          Useless Redux Button
          {!loading &&
            clicks > 0 && (
              <Label as="a" basic pointing="left">
                {`You clicked me ${clicks} time${clicks === 1 ? `` : `s`} and wasted ${
                  clicks > 1 ? `another` : ``
                } ${timer}ms`}
              </Label>
            )}
        </Button>
      </div>
    </div>
  </Container>
);

export default Application;
