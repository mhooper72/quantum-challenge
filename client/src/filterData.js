export const stringConstraints = [
  `starts with`,
  `does not start with`,
  `equals`,
  `does not equal`,
  `contains`,
  `does not contain`,
  `in list`,
  `not in list`,
  `contains all`
];
export const numericConstraints = [
  `range`,
  `less than or equal to`,
  `equals`,
  `does not equal`,
  `greater than or equal`
];

export const placeholderStringFields = {
  columnId: 'id',
  constraint: 'starts with',
  constraintValue: ''
};

export const getConstraintsForColumnId = columnId => {
  switch (columnId) {
    // list of all columnIds numbers
    case `visits`:
    case `screen_width`:
    case `screen_height`:
    case `page_response`:
      return numericConstraints;
    //  Catch all for anything other than numeric columnIds (strings)
    default:
      return stringConstraints;
  }
};

export const stringFields = [
  'id',
  'user_email',
  'user_first_name',
  'user_last_name',
  'domain',
  'path'
];

export const fields = [
  {
    displayName: 'ID',
    columnId: 'id'
  },
  {
    displayName: 'User Email',
    columnId: 'user_email'
  },
  {
    displayName: 'First Name',
    columnId: 'user_first_name'
  },
  {
    displayName: 'Last Name',
    columnId: 'user_last_name'
  },
  {
    displayName: 'Screen Width',
    columnId: 'screen_width'
  },
  {
    displayName: 'Screen Height',
    columnId: 'screen_height'
  },
  {
    displayName: '# of Visits',
    columnId: 'visits'
  },
  {
    displayName: 'Page Response Time (ms)',
    columnId: 'page_response'
  },
  {
    displayName: 'Domain',
    columnId: 'domain'
  },
  {
    displayName: 'Page',
    columnId: 'path'
  }
];
