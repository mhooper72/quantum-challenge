import { connect } from 'react-redux';
import Application from '../components/Application';
import { buildString } from '../store/stringBuilder/stringBuilder.actions';

export default connect(
  ({ stringBuilder }) => ({ stringBuilder }),
  {
    buildString
  }
)(Application);
