import { combineReducers } from 'redux';
import stringBuilderReducer from './stringBuilder/stringBuider.reducer';

const rootReducer = combineReducers({
  stringBuilder: stringBuilderReducer
});

export default rootReducer;
