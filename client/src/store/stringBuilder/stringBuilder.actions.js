export const BUILD_STRING_REQUEST = `BUILD_STRING_REQUEST`;
const buildStringRequest = () => ({
  type: BUILD_STRING_REQUEST
});

export const BUILD_STRING_SUCCESS = `BUILD_STRING_SUCCESS`;
const buildStringSuccess = randomNum => ({
  type: BUILD_STRING_SUCCESS,
  payload: randomNum
});

export const BUILD_STRING_ERROR = `BUILD_STRING_ERROR`;
const buildStringError = error => ({
  type: BUILD_STRING_ERROR,
  payload: error
});

export const buildString = () => async dispatch => {
  dispatch(buildStringRequest());
  const randomNum = Math.floor(Math.random() * 1000);
  try {
    setTimeout(() => {
      dispatch(buildStringSuccess(randomNum));
    }, randomNum);
  } catch (error) {
    dispatch(buildStringError(error));
  }
};
