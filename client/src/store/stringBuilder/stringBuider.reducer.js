import {
  BUILD_STRING_REQUEST,
  BUILD_STRING_SUCCESS,
  BUILD_STRING_ERROR
} from './stringBuilder.actions';

const initialState = {
  loading: false,
  clicks: 0
};

export default function stringBuilderReducer(state = initialState, action) {
  const { payload, type } = action;
  switch (type) {
    case BUILD_STRING_REQUEST:
      return { ...state, loading: true };

    case BUILD_STRING_SUCCESS:
      return {
        loading: false,
        timer: payload,
        clicks: state.clicks + 1
      };

    case BUILD_STRING_ERROR:
      return {
        loading: false,
        error: payload
      };
    default:
      return state;
  }
}
